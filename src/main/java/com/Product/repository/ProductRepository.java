package com.Product.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Product.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

	public List<Product> findByProductNameOrProductDescription(String name, String description);

	public List<Product> findByProductNameLike(String name);

	public List<Product> findByProductNameStartingWith(String name);

	public List<Product> getProductByProductNameAndProductReleaseDate(String name, Date date);

	public List<Product> getProductByProductNameAndPrice(String name, int price);

	public List<Product> getProductByProductNameOrPrice(String name, int price);

	public List<Product> findByPriceGreaterThan(int price);

	public Product findByProductName(String name);

}
