package com.Product.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Product.model.Login;

public interface LoginRepository extends JpaRepository<Login, Integer> {
	
	public Login findByUserNameAndPassword(String name,String password);

}
