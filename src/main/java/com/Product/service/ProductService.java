package com.Product.service;

import java.util.Date;
import java.util.List;

import com.Product.model.Product;

public interface ProductService {
	public List<Product> findByProductNameOrProductDescription(String name, String description);

	public Product saveProduct(Product product);

	public Product updateProduct(Product product);

	public void deleteProductById(int id);

	public Product getProductById(int id);

	public List<Product> getAllProducts();

	public List<Product> findByNameLike(String name);

	public List<Product> findByNameStartingWith(String name);

	public List<Product> findByNameAndDate(String name, Date date);

	public List<Product> getProductByProductNameAndPrice(String name, int price);

	public List<Product> getProductByProductNameOrPrice(String name, int price);

	public List<Product> findByPriceGreaterThan(int price);

	public Product findByName(String name);
	

}
