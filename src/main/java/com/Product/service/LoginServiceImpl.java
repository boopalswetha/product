package com.Product.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Product.model.Login;
import com.Product.repository.LoginRepository;

@Service
public class LoginServiceImpl implements LoginService{
    @Autowired
	private LoginRepository loginRepository;
	@Override
	public boolean login(String userName, String password) {
	Login login=loginRepository.findByUserNameAndPassword(userName, password);
	while(login!=null) {
		if(login.getUserName().equals(userName)&&(login.getPassword().equals(password))) {
		return true;
		}
	}
	//throw new InvalidUserException();
	return false;
	}
	@Override
	public Login addUser(Login login) {
		return loginRepository.saveAndFlush(login);
	}
	

}