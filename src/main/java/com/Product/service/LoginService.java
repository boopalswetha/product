package com.Product.service;

import com.Product.model.Login;

public interface LoginService {
	
	public boolean login(String userName,String password);

	public Login addUser(Login login);
}