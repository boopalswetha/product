package com.Product.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Product.exceptions.InvalidDateException;
import com.Product.exceptions.InvalidNameException;
import com.Product.exceptions.InvalidPriceException;
import com.Product.exceptions.ProductNotFoundException;
import com.Product.model.Product;
import com.Product.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductRepository productRepository;

	@Override
	public List<Product> findByProductNameOrProductDescription(String name, String description) {
		return productRepository.findByProductNameOrProductDescription(name, description);
	}

	@Override
	public Product saveProduct(Product product) {
		if ((product.getProductName() != null) && (product.getProductName().chars().allMatch(Character::isLetter))) {
			return productRepository.saveAndFlush(product);
		}
		throw new InvalidNameException();
	}

	@Override
	public Product updateProduct(Product product) {
		Date date = java.util.Calendar.getInstance().getTime();
		if (product.getProductReleaseDate().before(date)) {
			return productRepository.saveAndFlush(product);
		}
		throw new InvalidDateException();
	}

	@Override
	public void deleteProductById(int id) {
		productRepository.deleteById(id);
	}

	@Override
	public Product getProductById(int id) {
		return productRepository.getOne(id);
	}

	@Override
	public List<Product> getAllProducts() {
		return productRepository.findAll();
	}

	@Override
	public List<Product> findByNameLike(String name) {
		return productRepository.findByProductNameLike(name);
	}

	@Override
	public List<Product> findByNameStartingWith(String name) {
		return productRepository.findByProductNameStartingWith(name);
	}

	@Override
	public List<Product> findByNameAndDate(String name, Date date) {
		return productRepository.getProductByProductNameAndProductReleaseDate(name, date);
	}

	@Override
	public List<Product> getProductByProductNameAndPrice(String name, int price) {
		return productRepository.getProductByProductNameAndPrice(name, price);
	}

	@Override
	public List<Product> getProductByProductNameOrPrice(String name, int price) {
		return productRepository.getProductByProductNameOrPrice(name, price);
	}

	@Override
	public List<Product> findByPriceGreaterThan(int price) {
		if (price > 0 && price < 500000) {
			return productRepository.findByPriceGreaterThan(price);
		}
		throw new InvalidPriceException();
	}

	@Override
	public Product findByName(String name) {
		Product product = productRepository.findByProductName(name);
		if (product != null) {
			return product;
		}
		throw new ProductNotFoundException();

	}

}
