package com.Product.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "productId")
	private int productId;
	@Column(name = "productName", unique = true, nullable = false)
	private String productName;
	@Column(name = "productDescription", nullable = false)
	private String productDescription;
	@Column(nullable = false)
	private int price;
	@Column(name = "productReleaseDate", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date productReleaseDate;

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public Date getProductReleaseDate() {
		return productReleaseDate;
	}

	public int getPrice() {
		return price;
	}

	public Product(String productName, String productDescription, int price, Date productReleaseDate) {
		super();
		this.productName = productName;
		this.productDescription = productDescription;
		this.price = price;
		this.productReleaseDate = productReleaseDate;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setProductReleaseDate(Date productReleaseDate) {
		this.productReleaseDate = productReleaseDate;
	}

	public Product() {
		super();
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productDescription="
				+ productDescription + ", price=" + price + ", productReleaseDate=" + productReleaseDate + "]";
	}

}
