package com.Product.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ProductExceptionHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler(value = ProductNotFoundException.class)
	public ResponseEntity<Object> exception(ProductNotFoundException exception) {
		return new ResponseEntity<>("product not found", HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = InvalidPriceException.class)
	public ResponseEntity<Object> exception(InvalidPriceException exception) {
		return new ResponseEntity<>("invalid price, Price is not greater than 500000 ",
				HttpStatus.NOT_ACCEPTABLE);
	}

	@ExceptionHandler(value = InvalidDateException.class)
	public ResponseEntity<Object> exception(InvalidDateException exception) {
		return new ResponseEntity<>("Invalid date, You must Enter the past date ", HttpStatus.EXPECTATION_FAILED);
	}
	@ExceptionHandler(value = InvalidNameException.class)
	public ResponseEntity<Object> exception(InvalidNameException exception) {
		return new ResponseEntity<>("Invalid Name, You must Enter only alphabets ", HttpStatus.NOT_ACCEPTABLE);
	}
	@ExceptionHandler(value = InvalidUserException.class)
	public ResponseEntity<Object> exception(InvalidUserException exception) {
		return new ResponseEntity<>("user not logged in", HttpStatus.NOT_ACCEPTABLE);
	}

}

