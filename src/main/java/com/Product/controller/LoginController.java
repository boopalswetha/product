package com.Product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Product.model.Login;
import com.Product.service.LoginService;

@RestController
public class LoginController {
	@Autowired
	private LoginService loginService;
	@PostMapping(value="/login")
	public ResponseEntity<Login> addUser(@RequestBody Login login){
		
		Login login2=loginService.addUser(login);
		return new ResponseEntity<Login>(login2,HttpStatus.OK);
		
	}
	@PostMapping(value="/login2")
	public ResponseEntity<Boolean> login(@RequestParam String userName,@RequestParam String password){
		boolean name=loginService.login(userName, password);
		return new ResponseEntity<Boolean>(name, HttpStatus.OK);
	}
}