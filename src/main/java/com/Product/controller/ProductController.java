package com.Product.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Product.model.Product;
import com.Product.service.LoginService;
import com.Product.service.ProductService;

@RestController
public class ProductController {
	@Autowired
	private ProductService productService;
	@Autowired
	private LoginService loginService;
	boolean result = false;

	@PostMapping(value = "/login3")
	public ResponseEntity<Boolean> login(@RequestParam String userName, @RequestParam String password) {
		result = loginService.login(userName, password);

		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
	}

	@GetMapping(value = "/product")
	public ResponseEntity<List<Product>> getNameOrDescription(@RequestParam String name,
			@RequestParam String description) {
		List<Product> products = productService.findByProductNameOrProductDescription(name, description);
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);

	}

	@GetMapping(value = "/products")
	public ResponseEntity<Object> getAllProducts() {
		List<Product> products = null;
		if (result) {
			products = productService.getAllProducts();

			return new ResponseEntity<Object>(products, HttpStatus.OK);
		}
		return new ResponseEntity<Object>("user not logged in", HttpStatus.NOT_ACCEPTABLE);

	}

	@GetMapping(value = "/product/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable int id) {
		Product product = productService.getProductById(id);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

	@PostMapping(value = "/product")
	public ResponseEntity<Product> saveProduct(@RequestBody Product product) {
		Product product1 = productService.saveProduct(product);
		return new ResponseEntity<Product>(product1, HttpStatus.OK);

	}

	@PutMapping(value = "/product")
	public ResponseEntity<Product> saveOrUpdateProduct(@RequestBody Product product) {
		Product product1 = productService.updateProduct(product);
		return new ResponseEntity<Product>(product1, HttpStatus.OK);
	}

	@DeleteMapping(value = "/product/{id}")
	public ResponseEntity<String> deleteProduct(@PathVariable int id) {
		productService.deleteProductById(id);
		return new ResponseEntity<String>("deleted successfully", HttpStatus.OK);
	}

	@GetMapping(value = "/product1/{productname}")
	public ResponseEntity<List<Product>> getProdyctByNmaeLike(@PathVariable("productname") String name) {
		List<Product> products = productService.findByNameLike(name);
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}

	@GetMapping(value = "/product2")
	public ResponseEntity<List<Product>> getProductByNameStartingWith(@RequestParam String name) {
		List<Product> products = productService.findByNameStartingWith(name);
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}

	@GetMapping(value = "/product3")
	public ResponseEntity<List<Product>> getByNameAndDate(@RequestParam String name,
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
		List<Product> products = productService.findByNameAndDate(name, date);
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}

	@GetMapping(value = "/product4")
	public ResponseEntity<List<Product>> getByNameAndPrice(@RequestParam String name, @RequestParam int price) {
		List<Product> products = productService.getProductByProductNameAndPrice(name, price);
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}

	@GetMapping(value = "/product5")
	public ResponseEntity<List<Product>> getByNameOrPrice(@RequestParam String name, @RequestParam int price) {
		List<Product> products = productService.getProductByProductNameOrPrice(name, price);
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}

	@GetMapping(value = "/product6")
	public ResponseEntity<List<Product>> getPriceGreaterThan(@RequestParam int price) {
		List<Product> products = productService.findByPriceGreaterThan(price);
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}

	@GetMapping(value = "/product7/{name}")
	public ResponseEntity<Product> findByProductName(@PathVariable String name) {
		Product product = productService.findByName(name);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

}
