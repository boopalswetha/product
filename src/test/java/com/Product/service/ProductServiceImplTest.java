package com.Product.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.Product.exceptions.InvalidNameException;
import com.Product.exceptions.ProductNotFoundException;
import com.Product.model.Product;
import com.Product.repository.ProductRepository;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {
	@InjectMocks
	ProductServiceImpl productServiceImpl;
	@Mock
	ProductRepository productRepository;

	static Product product = null;

	@BeforeClass
	public static void setUp() {
		product = new Product();

	}

	@Test
	public void testGetByIdForPositive() {
		product.setProductId(2);
		product.setProductName("lenovo");
		Mockito.when(productRepository.getOne(Mockito.anyInt())).thenReturn(product);

		Product resultProduct = productServiceImpl.getProductById(1);
		Assert.assertNotNull(resultProduct);
		Assert.assertEquals("lenovo", resultProduct.getProductName());
	}

	@Test
	public void testGetAllProductsForPositive() {
		List<Product> products = new ArrayList();

		product.setProductId(1);
		product.setPrice(5000);
		product.setProductName("SonyReader");
		products.add(product);
		product.setProductId(2);
		product.setPrice(25000);
		product.setProductName("SonyTV");
		products.add(product);
		product.setProductId(3);
		product.setPrice(55000);
		product.setProductName("SonyTV");
		products.add(product);
		Mockito.when(productRepository.findAll()).thenReturn(products);
		Assert.assertEquals(3, products.size());

	}

	@Test(expected = ProductNotFoundException.class)
	public void testFindByNameForException() {
		product.setProductId(1);
		product.setProductName("samsung");
		Mockito.when(productRepository.findByProductName("samsung")).thenReturn(product);
		Product result = productServiceImpl.findByName("LG");
	}

	@Test(expected=InvalidNameException.class)
	public void testSaveForException() {

		product.setProductId(12);
		product.setProductName("89w2y8iyid");
		Mockito.when(productRepository.saveAndFlush(product)).thenReturn(product);
		Product result = productServiceImpl.saveProduct(product);

	}

	@Test(expected=NullPointerException.class)
	public void testUpdateForNegative() {
		product.setProductId(-9090);
		product.setProductName("Nokia");
		Mockito.when(productRepository.saveAndFlush(product)).thenReturn(product);
		Product result = productServiceImpl.updateProduct(product);
		Assert.assertNotNull(result);
	}
	
	
	@Test
	public void testFindByNameAndPrice() {
		List<Product> products=new ArrayList();
		product.setProductId(2);
		product.setPrice(7800);
		product.setProductName("phone");
		products.add(product);
		product.setProductId(2);
		product.setPrice(7800);
		product.setProductName("phone");
		products.add(product);
		
		Mockito.when(productRepository.getProductByProductNameAndPrice("phone", 7800)).thenReturn(products);
		List<Product> result=productServiceImpl.getProductByProductNameAndPrice("phone", 7800);
		Assert.assertEquals(2,result.size());
		
		
	}

	@AfterClass
	public static void tearDown() {
		product = null;
	}
}
