package com.Product.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.Product.model.Product;
import com.Product.service.ProductServiceImpl;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {

	static Product product = null;

	@InjectMocks
	ProductController productController;
	@Mock
	ProductServiceImpl productServiceImpl;

	@BeforeClass
	public static void setUp() {
		product = new Product();
	}

	@Test
	public void testSaveForNegetive() {

		product.setProductId(2);
		product.setPrice(-88);
		Mockito.when(productServiceImpl.saveProduct(product)).thenReturn(product);
		ResponseEntity<Product> result = productController.saveProduct(product);
		Assert.assertNotNull(product);
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());

	}

	@Test
	public void testSaveForException() {

		product.setProductName("vivo");
		product.setPrice(70000);
		Mockito.when(productServiceImpl.saveProduct(product)).thenReturn(product);
		ResponseEntity<Product> product2 = productController.saveProduct(product);

	}

	@Test
	public void testProductNameStartingWith() {

		List<Product> products = new ArrayList();

		product.setProductName("LGTV");
		products.add(product);
		product.setProductName("LGFridge");
		products.add(product);
		Mockito.when(productServiceImpl.findByNameStartingWith("LG")).thenReturn(products);
		ResponseEntity<List<Product>> result = productController.getProductByNameStartingWith("Samsung");
		Assert.assertNotNull(result);

	}

	@Test
	public void testPriceGreaterThanForNegative() {
		List<Product> products = new ArrayList();
		product.setProductName("LGTV");
		product.setPrice(9000);
		products.add(product);
		product.setProductName("LGFridge");
		product.setPrice(10000);
		products.add(product);
		Mockito.when(productServiceImpl.findByPriceGreaterThan(-88)).thenReturn(products);
		ResponseEntity<List<Product>> result = productController.getPriceGreaterThan(-88);
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());

	}

	@Test
	public void testFindByProductName() {
		product.setProductName("lenovo");
		Mockito.when(productServiceImpl.findByName("j%$jnjk")).thenReturn(product);
		ResponseEntity<Product> result = productController.findByProductName("j%$jnjk");
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
	}

	@Test
	public void testSaveOrUpdate() {

		product.setProductId(12);
		product.setProductName("hvm");
		Mockito.when(productServiceImpl.updateProduct(product)).thenReturn(product);
		ResponseEntity<Product> product1 = productController.saveOrUpdateProduct(product);
		Assert.assertEquals(product1, product1);

	}

	@Test
	public void testSaveOrUpdateForInvalid() {

		product.setProductId(19);
		product.setProductName("hvm$%^");
		Mockito.when(productServiceImpl.updateProduct(product)).thenReturn(product);
		ResponseEntity<Product> product1 = productController.saveOrUpdateProduct(product);
		Assert.assertEquals(product1, product1);

	}

	@AfterClass
	public static void tearDown() {

		product = null;

	}

}

